<?php
class Route
{
  public static function start()
  {
    /*
    * Set the default property of controller and action
    */
    $controller_name = 'Main';
    $action_name = 'index';

    $routes = explode('/', $_SERVER['REQUEST_URI']);
    /*
    * Get controller name and action name
    */
    if (!empty($routes[1])) {
        $controller_name = $routes[1];
    }
    if (!empty($routes[2])) {
        $action_name = $routes[2];
    }
    /*
    * Add prefix
    */
    $model_name = 'Model'.$controller_name;
    $controller_name = 'Controller'.$controller_name;

    $model_file = $model_name.'.php';
    $model_path = "app/models/".$model_file;

    if (file_exists($model_path)) {
        include "app/models/".$model_file;
    }
    $controller_file = $controller_name.'.php';
    $controller_path = "app/controllers/".$controller_file;

    if (file_exists($controller_path)) {
        include "app/controllers/".$controller_file;
    } else {
        Route::error404();
    }

    $controller = new $controller_name;
    $action = $action_name;

    if (method_exists($controller,$action)) {
        $controller->$action();
    } else {
        Route::error404();
    }
  }
  public function error404()
  {
    header('HTTP/1.1 404 Not Found');
    header('Status: 404 Not Found');
    header('Location:http://'.$_SERVER['HTTP_HOST']);
  }
}
