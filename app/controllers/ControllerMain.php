<?php
class ControllerMain extends Controller
{
    public function __construct()
    {
        $this->model = new ModelMain();
        $this->view = new View();
    }
    public function index()
    {
        $data = $this->model->getTables();
        if (count($data) == 0) {
            $this->model->createStructure();
            $data = $this->model->getAllData();
        } else {
            $data = $this->model->getAllData();
        }
        $this->view->render('main_view.php', 'template_view.php', $data);
    }
    public function sort()
    {
        $data = $this->model->getSortedData();
        $this->view->render('main_view.php', 'template_view.php', $data);
    }
    public function search()
    {
        $data = $this->model->searchData($_POST['srch']);
        $this->view->render('main_view.php', 'template_view.php', $data);
    }
    public function add()
    {
        $name = strip_tags($_POST['name']);
        $name = htmlentities($name, ENT_QUOTES, "UTF-8");
        $name = htmlspecialchars($name, ENT_QUOTES);

        $year = strip_tags($_POST['year']);
        $year = htmlentities($year, ENT_QUOTES, "UTF-8");
        $year = htmlspecialchars($year, ENT_QUOTES);

        $format = strip_tags($_POST['format']);
        $format = htmlentities($format, ENT_QUOTES, "UTF-8");
        $format = htmlspecialchars($format, ENT_QUOTES);

        $actors = strip_tags($_POST['actors']);
        $actors = htmlentities($actors, ENT_QUOTES, "UTF-8");
        $actors = htmlspecialchars($actors, ENT_QUOTES);

        $this->model->addFilm($name, $year, $format, $actors);
        echo "success";
    }
    public function del()
    {
        $this->model->delFilm($_POST['id']);
        echo "success";
    }
    public function import()
    {
        $uploaddir = "src/files/";
        $uploadfile = $uploaddir . basename($_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
            echo "import success";
            $this->model->import($uploadfile);
        } else {
            echo "Oшибка! Некоторая отладочная информация: files-".serialize($_FILES);
        }
    }
}
