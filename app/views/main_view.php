<div class="container">
    <div class="list">
        <a href="Main/sort">Сортировать все 0-9, А-Я</a>
        <form action="/Main/search" method="POST" enctype="application/x-www-form-urlencoded">
            <label class="label-text">
                <input type="text" name="srch" placeholder=" " class="input-text">
                <span class="span-input-text">Введите название фильма или имя актера</span>
            </label>
            <!-- <input type="text" name="srch" placeholder="Введите название фильма или имя актера"> -->
            <button>Искать</button>
        </form>
        <?php
            if (count($data) == 0) {
                echo "<h2>Добро пожаловать, записей еще нет, хотите добавить?</h2>";
            } else {
                foreach ($data as $value) {
                ?>
        <div class="item">
            <h4>Название: <?=$value[1]?></h4>
            <ul>
                <li>Год выпуска: <?=$value[2]?></li>
                <li>Формат: <?=$value[3]?></li>
                <li>Список актеров: <?=$value[4]?></li>
                <li><button class="destroyer" data="<?=$value[0]?>">Удалить</button></li>
            </ul>
        </div>
                <?php
                }
            }
        ?>
        <form>
            <label class="label-text">
                <input type="text" id="name" name="name" placeholder=" " class="input-text" required>
                <span class="span-input-text">Название фильма</span>
            </label>
            <label class="label-text">
                <input type="text" id="year" name="year" placeholder=" " class="input-text" required>
                <span class="span-input-text">Год (4 цифры)</span>
            </label>
            <select name="format" id="format" class="select2" required>
                <option>DVD</option>
                <option>VHS</option>
                <option>Blu-Ray</option>
            </select>
            <label class="label-text">
                <textarea type="text" id="actors" name="actors" placeholder=" " class="input-text" required></textarea>
                <span class="span-input-text">Актеры</span>
            </label>
            <button id="formSender">Добавить</button>
        </form>
        <form id="fileUpload" enctype="multipart/form-data" method="post">
            <h3>Импортировать с файла</h3>
            <input type="file" name="file" required />
            <button>Импортировать</button>
        </form>
        <p>В формате:<br>
            Title: <br>
            Release <br>
            Format: <br>
            Stars:
        </p>
    </div>
</div>
<script>
    document.querySelectorAll('.destroyer').forEach(function(element) {
        element.onclick = function(){
            let body = 'id='+this.getAttribute('data');
            let src = '/Main/del';
            let contentType = 'application/x-www-form-urlencoded';
            request(src, body, contentType);
        };
    });

    document.getElementById('formSender').onclick = function(evt){
        evt.preventDefault();
        let name = document.getElementById('name').value;
        let year = Number(document.getElementById('year').value);
        let format = document.getElementById('format').value;
        let actors = document.getElementById('actors').value;
        if (name != '' && year != NaN && year < 2050) {
            let src = '/Main/add';
            let contentType = 'application/x-www-form-urlencoded';
            let body = 'name='+name+'&year='+year+'&format='+format+'&actors='+actors;
        request(src, body, contentType);
        } else {
            alert("Вы неправильно заполнили необходимые поля!");
        }
    };

    let form = document.getElementById('fileUpload');
    form.addEventListener('submit', function(evt) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/Main/import', true);
        xhr.send(new FormData(form));
        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;
            if (xhr.status != 200) {
                alert(xhr.status + ': ' + xhr.statusText);
            } else {
                // alert(xhr.responseText);
                location.reload();
            }
        }
        evt.preventDefault();
    }, false);

    function request(src, body, contentType){
        var xhr = new XMLHttpRequest();
        xhr.open('POST', src, true);
        xhr.setRequestHeader('Content-Type', contentType);
        xhr.send(body);
        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;
            if (xhr.status != 200) {
                alert(xhr.status + ': ' + xhr.statusText);
            } else {
                location.reload();
            }
        }
    }
</script>