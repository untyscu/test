<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8"/>
  <title>Test app</title>
  <link rel="stylesheet" type="text/css" href="/src/css/main.css">
  <link href="/src/css/select2.min.css" rel="stylesheet" />
</head>
<body>
  <?php include './app/views/'.$content_view;?>
  <script src="/src/js/jquery-3.2.1.min.js"></script>
  <script src="/src/js/select2.min.js"></script>
  <script src="/src/js/main.js"></script>
</body>
</html>
