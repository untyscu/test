<?php
/**
 * Created by PhpStorm.
 * User: SmartDev_Untyscu
 * Date: 20.09.2018
 * Time: 13:29
 */

class DB
{
    private $db_server = "localhost";
    private $db_user = "myuser";
    private $db_password = "testpass";
    private $db_database = "test";
    private static $_instance;

    private $db;

    public static function getInstance()
    {
        if(!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
        if (file_exists("src/files/db.conf")) {
            $confArray = explode("|", file_get_contents("src/files/db.conf"));
            $this->db_server = $confArray[0];
            $this->db_database = $confArray[1];
            $this->db_user = $confArray[2];
            $this->db_password = $confArray[3];
            // var_dump($confArray);
            // exit;
        }
        $this->db = new mysqli($this->db_server, $this->db_user, $this->db_password, $this->db_database);
        if ($this->db->connect_error) {
            die('Connect Error: ' . $this->db->connect_error);
        }
        $this->db->query("SET NAMES 'utf8'");
    }

    private function __clone()
    {
        // TODO: Implement __clone() method.
    }

    public function getConnection() {
        return $this->db;
    }
}
