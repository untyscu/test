<?php
class ModelMain extends Model
{
    public function createStructure()
    {
        $result = $this->mysqli->query("CREATE TABLE films(id INTEGER AUTO_INCREMENT PRIMARY KEY, name VARCHAR(150), year VARCHAR(12), format VARCHAR(50), actors TEXT)");
        if ($this->mysqli->error) {
            var_dump($this->mysqli->error);
            die();
        };
    }
    public function getTables()
    {
        $result = $this->mysqli->query("SHOW TABLES");
        if ($this->mysqli->error) {
            var_dump($this->mysqli->error);
            die();
        }
        return $result->fetch_all();
    }
    public function getAllData()
    {
        $result = $this->mysqli->query("SELECT * FROM `films`");
        if ($this->mysqli->error) {
            var_dump($this->mysqli->error);
            die();
        }
        return $result->fetch_all();
    }
    public function searchData($srch)
    {
        $result = $this->mysqli->query("SELECT * FROM `films` WHERE `name` LIKE '%".$srch."%' OR `actors` LIKE '%".$srch."%'");
        if ($this->mysqli->error) {
            var_dump($this->mysqli->error);
            die();
        }
        return $result->fetch_all();
    }
    public function getSortedData()
    {
        $result = $this->mysqli->query("SELECT * FROM films ORDER BY name");
        if ($this->mysqli->error) {
            var_dump($this->mysqli->error);
            die();
        }
        return $result->fetch_all();
    }
    public function addFilm($name, $year, $format, $actors)
    {
    $result = $this->mysqli->query("INSERT INTO `films` (name, year, format, actors) VALUES ('".$name."', '".$year."', '".$format."', '".$actors."')");
        if ($this->mysqli->error) {
            var_dump($this->mysqli->error);
            die();
        };
    }
    public function delFilm($id)
    {
        $result = $this->mysqli->query("DELETE FROM `films` WHERE `id`= '$id'");
        if ($this->mysqli->error) {
            var_dump($this->mysqli->error);
            die();
        };
    }
    public function import($uploadfile)
    {
        $matches = [];
        preg_match_all("/Title:(.*)\nRelease Year:(.*)\nFormat:(.*)\nStars:(.*)\n/", file_get_contents($uploadfile), $matches);
        if (count($matches[0]) != 0) {
            for ($i=0; $i < count($matches[1]); $i++) {
                $name = $matches[1][$i];
                $name = strip_tags($name);
                $name = htmlentities($name, ENT_QUOTES, "UTF-8");
                $name = htmlspecialchars($name, ENT_QUOTES);
                if ($name == '' || $name == ' ') {
                    $name = "Упс, похоже что здесь неправильные данные";
                }

                $year = $matches[2][$i];
                $year = strip_tags($year);
                $year = htmlentities($year, ENT_QUOTES, "UTF-8");
                $year = htmlspecialchars($year, ENT_QUOTES);
                if (!intval($year)) {
                    $year = "ERROR=)";
                }

                $format = $matches[3][$i];
                $format = strip_tags($format);
                $format = htmlentities($format, ENT_QUOTES, "UTF-8");
                $format = htmlspecialchars($format, ENT_QUOTES);
                $format_matches = [];
                preg_match_all("/DVD|VHS|Blu-Ray/", $format, $format_matches);
                if ($format_matches[0][0] == '') {
                    $format = "ERROR=)";
                }
                echo $format_matches[0][0];
                echo "\n";

                $actors = $matches[4][$i];
                $actors = strip_tags($actors);
                $actors = htmlentities($actors, ENT_QUOTES, "UTF-8");
                $actors = htmlspecialchars($actors, ENT_QUOTES);
                if ($actors == '') {
                    $actors = "Упс, похоже что здесь неправильные данные";
                }

                $this->addFilm($name, $year, $format, $actors);
            }
        } else {
            echo "Упс, кажется в файле нет информации о фильмах. Проверьте загрузочный файл!";
        }
    }
}
