Для установки данного приложения потребуется сервер apache или nginx, 
php7.0 или старше с расширением mysqli, mysql сервер с пустой БД 
кодировкой utf8_general_ci
    
**Настройка сервера**

Так как используется паттерн 'Front controller' необходимо перенаправить все 
запросы на index.php, сделать можно следующим образом:

Если у вас apache в настройках доступа к корневой директории должна быть строка
    
`AllowOverride ALL`

Включите mod rewrite

`a2enmod rewrite`

и в корне директории проэкта поместите файл .htaccess с таким контентом

```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule .* index.php [L]
```


Если у вас nginx в вашем nginx.conf должен быть параметр 
```
location / {
    try_files $uri $uri/ /index.php;
}
```

    
**Подключение БД**

В файле src/files/db.conf пропишите следующее

`хост|имя базы данных|имя пользователя|пароль`